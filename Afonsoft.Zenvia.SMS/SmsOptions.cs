﻿#if NETCORE
using Microsoft.Extensions.Options;
#endif

namespace Afonsoft.Zenvia.SMS
{
#if NETCORE
    public class SmsOptions : IOptions<SmsOptions>
#else
    public class SmsOptions
#endif
    {
        /// <summary>
        /// This object
        /// </summary>
        public SmsOptions Value => this;

        /// <summary>
        /// Tamanho maximo da mensagem considerando "from"
        /// </summary>
        public int BODY_MAX_LENGTH => 150;

        /// <summary>
        /// Tamanho maximo do ID
        /// </summary>
        public int ID_MAX_LENGTH => 20;

        /// <summary>
        /// Caso a sua conta possua a configuração de agrupadores habilitada será necessário informar o id do agrupador no momento do envio.
        /// </summary>
        public int? AggregateId { get; set; }

        /// <summary>
        /// Account for Send SMS (zenvia)
        /// </summary>
        public string Account { get; set; }
        
        /// <summary>
        /// Password for Send SMS (zenvia)
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Timeout 
        /// </summary>
        public int Timeout { get; set; } = 240000;

        /// <summary>
        /// EndPoint
        /// </summary>
        public string EndPoint { get; set; } = "https://api-rest.zenvia.com/services";

    }
}
