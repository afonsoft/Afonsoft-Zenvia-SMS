﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Afonsoft.Zenvia.SMS
{
    /// <summary>
    /// Calsse principal para conexão com a API do zemvia
    /// http://www.zenvia.com.br/pt/envio-de-sms/
    /// http://docs.zenviasms.apiary.io/#introduction/tabela-de-status
    /// http://system.human.com.br/report/main.jsp 
    /// http://www.zenvia360.com.br/system/ 
    /// </summary>
    public class Service : IDisposable
    {
        //Conta: avianca.e2s
        //Senha: 75ZESp9kfr
        //E-Mail:
        //http://system.human.com.br/report/main.jsp 
        //http://www.zenvia360.com.br/system/ 

        public SmsOptions Options { get; set; }
        private CookieContainer _cookieContainer;

        private static SmsOptions Build(Action<SmsOptions> options)
        {
            var opt = new SmsOptions();
            options.Invoke(opt);
            return opt;
        }

        public Service(Action<SmsOptions> options)
        {
            Options = Build(options);
            _cookieContainer = new CookieContainer();
        }

        #region Serialize Deserialize
        private string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }).Replace(Environment.NewLine, "").Replace("\n", "").Replace("\r", "");
        }

        private T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        #endregion

        #region Http

        private string HttpGet(string uri)
        {
            return Http(uri, "GET");
        }

        private string HttpPost(string uri)
        {
            return Http(uri, "POST");
        }

        private string HttpPost(string uri, string postData)
        {
            return Http(uri, "POST", postData);
        }

        private string Http(string uri, string method, string postData = null)
        {
            var result = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(Options.EndPoint + uri));
                request.KeepAlive = true;
                request.Timeout = 60000;
                request.Credentials = new NetworkCredential(Options.Account, Options.Password);
                String encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(Options.Account + ":" + Options.Password));
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.CookieContainer = _cookieContainer;
                request.Method = method;
                request.ContentLength = 0;
                request.Accept = "application/json, application/xml, text/html, text/plain";

                if (request.Method == "POST")
                {
                    request.ContentType = "application/json";
                    if (!string.IsNullOrEmpty(postData))
                    {
                        byte[] postDataBytes = Encoding.UTF8.GetBytes(postData);
                        request.ContentLength = postDataBytes.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postDataBytes, 0, postDataBytes.Length);
                        }
                    }
                }

                using (var httpWebResponse = (HttpWebResponse)request.GetResponse())
                {
                    if (httpWebResponse != null)
                    {
                        _cookieContainer.Add(httpWebResponse.Cookies);
                        using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd().Trim().Replace(Environment.NewLine, "").Replace("\n", "").Replace("\r", "");
                        }
                    }
                }

            }
            catch (WebException wex)
            {
                var response = (HttpWebResponse)wex.Response;
                if (response != null)
                {
                    try
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            result = reader.ReadToEnd().Replace(Environment.NewLine, "").Replace("\n", "").Replace("\r", "");
                        }
                    }
                    catch
                    {
                        result = string.Format("A WebException '{0}' status '{1}'", wex.Message, wex.Status);
                        result += string.Format(", response: {0} {1}: {2}", (int)response.StatusCode, response.StatusDescription, wex.Message);
                    }
                }
                else
                {
                    result = string.Format("A WebException '{0}' status '{1}'", wex.Message, wex.Status);
                }
                throw new Exception(result, wex);
            }
            return result;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

        }

        protected void Dispose(bool disposing)
        {
            _cookieContainer = null;
            if (disposing)
                GC.SuppressFinalize(this);
        }

        #endregion



        /// <summary>
        /// Metodo para o envio de SMS
        /// </summary>
        /// <param name="sms">Objeto do tipo SmsEntity</param>
        /// <returns>retorna um objeto do tipo SmsResponse com o status e o detalhe da operação.</returns>
        public Response Send(Request sms)
        {
            var reply = new Response();
            try
            {
                if (sms == null) throw new ArgumentNullException(nameof(sms), "entity está nulo");

                if (string.IsNullOrEmpty(sms.Id))
                    throw new ArgumentNullException(nameof(sms), "entity.Id está nulo ou vazio.");

                if (string.IsNullOrEmpty(sms.Message))
                    throw new ArgumentNullException(nameof(sms), "entity.Message está nulo ou vazio.");

                //limitar a mensagem.
                if (sms.Message.Length > Options.BODY_MAX_LENGTH)
                    throw new ArgumentOutOfRangeException(nameof(sms), "Fields Message can not exceed " + Options.BODY_MAX_LENGTH + " characters.");

                //Limitar o ID
                if (sms.Id.Length > Options.ID_MAX_LENGTH)
                    throw new ArgumentOutOfRangeException(nameof(sms), "Field ID can not have more than " + Options.ID_MAX_LENGTH + " characters.");
                
                var response = Deserialize<SmsResponse>(HttpPost("/send-sms", Serialize(new SmsRequest() { Sms = sms })));

                if (response != null)
                {
                    reply.StatusCode = response.Sms.StatusCode;
                    reply.StatusDescription = response.Sms.StatusDescription;
                    reply.DetailCode = response.Sms.DetailCode;
                    reply.DetailDescription = response.Sms.DetailDescription;
                    reply.Parts = response.Sms.Parts;
                }
                else
                {
                    reply.StatusCode = "10";
                    reply.StatusDescription = "Error";
                    reply.DetailCode = "999";
                    reply.DetailDescription = "Object SmsSendReply is null";
                }
            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);
                reply.StatusCode = "10";
                reply.StatusDescription = "Error";
                reply.DetailCode = e == null ? "999" : "901";
                reply.DetailDescription = e?.Exception?.Message ?? ex.Message;
            }
            return reply;
        }

        /// <summary>
        /// Envio de vários SMSs simultaneamente.
        /// Este método recebe uma lista de objetos sendSmsRequest, mas neste caso o atributo aggregateId (caso necessário) deve estar fora do bloco individual, devendo ser o mesmo para todas as mensagens do request.
        /// </summary>
        /// <param name="multSms">Objeto do tipo SmsMultiEntity</param>
        /// <returns>retorna um objeto do tipo SmsMultiResponse com o status e o detalhe da operação.</returns>
        public MultResponse SendMulti(MultRequest multSms)
        {
            var reply = new MultResponse();
            try
            {
                if (multSms == null) throw new ArgumentNullException(nameof(multSms), "entity está nulo");

                if (multSms.Sms == null) throw new ArgumentNullException(nameof(multSms), "entity.Sms está nulo");

                if (multSms.Sms == null || multSms.Sms.Length <= 0) throw new ArgumentNullException(nameof(multSms), "entity.sendSmsRequestList está nulo");

                foreach (var sendSmsRequest in multSms.Sms)
                {
                    if (string.IsNullOrEmpty(sendSmsRequest.Id))
                        throw new ArgumentNullException(nameof(multSms), "entity.Id está nulo ou vazio.");

                    if (string.IsNullOrEmpty(sendSmsRequest.Message))
                        throw new ArgumentNullException(nameof(multSms), "entity.Message está nulo ou vazio.");

                    //limitar a mensagem.
                    if (sendSmsRequest.Message.Length > Options.BODY_MAX_LENGTH)
                        throw new ArgumentOutOfRangeException(nameof(multSms), "Fields Message can not exceed " + Options.BODY_MAX_LENGTH + " characters.");

                    //Limitar o ID
                    if (sendSmsRequest.Id.Length > Options.ID_MAX_LENGTH)
                        throw new ArgumentOutOfRangeException(nameof(multSms), "Field ID can not have more than " + Options.ID_MAX_LENGTH + " characters.");
                }

                var response = Deserialize<SmsMultResponse>(HttpPost("/send-sms-multiple", Serialize(new SmsMultRequest() { MultRequest = multSms })));
                if (response != null)
                {
                    reply.Sms = response.MultResponse.Sms;
                }
                else
                {
                    reply.Sms = new[]
                    {
                        new Response()
                        {
                            StatusCode = "10",
                            StatusDescription = "Error",
                            DetailCode = "999",
                            DetailDescription = "Object SmsMultReply is null"
                        }
                    };
                }

            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);
                reply.Sms = new[]
                {
                    new Response()
                    {
                        StatusCode = "10",
                        DetailCode = e == null ? "999" : "901",
                        StatusDescription = "Error",
                        DetailDescription = e?.Exception?.Message ?? ex.Message
                    }
                };
            }
            return reply;
        }

        /// <summary>
        /// Cancela o envio de um SMS previamente agendado. Para realizar o cancelamento, é necessário que tenha sido utilizado o identificador id no momento do envio.
        /// <remarks>Importante: o cancelamento de um SMS só pode ser feito até sua data de agendamento. Após, o SMS terá sido enviado à operadora e não poderá ser cancelado.</remarks>
        /// </summary>
        /// <param name="id">id da mensagem enviada</param>
        /// <returns>retorna um objeto do tipo SmsResponse com o status e o detalhe da operação.</returns>
        public Response Cancel(string id)
        {
            var reply = new Response();
            try
            {
                if (string.IsNullOrEmpty(id))
                    throw new ArgumentNullException("id", "Favor informar o Id da mensagem envaida.");

                if (id.Length >= Options.ID_MAX_LENGTH)
                    throw new ArgumentOutOfRangeException("id", "Field ID can not have more than " + Options.ID_MAX_LENGTH + " characters.");

                var response = Deserialize<SmsResponse>(HttpPost(string.Format("/cancel-sms/{0}", id)));
                if (response != null)
                {
                    reply.StatusCode = response.Sms.StatusCode;
                    reply.StatusDescription = response.Sms.StatusDescription;
                    reply.DetailCode = response.Sms.DetailCode;
                    reply.DetailDescription = response.Sms.DetailDescription;
                    reply.Parts = response.Sms.Parts;
                }
                else
                {
                    reply.StatusCode = "10";
                    reply.StatusDescription = "Error";
                    reply.DetailCode = "999";
                    reply.DetailDescription = "Object SmsCancelReply is null";
                }
            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);

                reply.StatusCode = "10";
                reply.StatusDescription = "Error";
                reply.DetailCode = e == null ? "999" : "901";
                reply.DetailDescription = e?.Exception?.Message ?? ex.Message;
            }
            return reply;
        }

        /// <summary>
        /// Retorna a lista de SMSs recebidos em um período definido.
        /// </summary>
        /// <param name="startDateTime">2014-08-22T00:00:00</param>
        /// <param name="endDateTime">2014-08-22T23:59:59</param>
        /// <returns>Retorna um objeto do tipo ReceivedResponse com o status e o detalhe das SMS</returns>
        public ReceivedResponse Search(DateTime startDateTime, DateTime endDateTime)
        {
            ReceivedResponse reply = new ReceivedResponse();
            try
            {
                if (startDateTime > endDateTime)
                    throw new ArgumentOutOfRangeException("startDateTime", startDateTime, "Start date greater than end date.");

                if ((endDateTime - startDateTime).TotalDays > 180)
                    throw new IndexOutOfRangeException("interval between date must be max. 180 days.");

                var response = Deserialize<SmsReceivedResponse>(HttpGet(string.Format("/received/search/{0:s}/{1:s}", startDateTime, endDateTime)));
                if (response != null)
                {
                    return response.Received;
                }
                else
                {
                    reply.Sms = new Response
                    {
                        StatusCode = "10",
                        StatusDescription = "Error",
                        DetailCode = "999",
                        DetailDescription = "Object ReceivedResponse is null"
                    };
                }
            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);
                reply.Sms = new Response
                {
                    StatusCode = "10",
                    StatusDescription = "Error",
                    DetailCode = e == null ? "999" : "901",
                    DetailDescription = e?.Exception?.Message ?? ex.Message
                };
            }
            return reply;
        }

        /// <summary>
        /// Listar Novos SMS recebidos.
        /// Retorna a lista de novos SMSs recebidos. Uma vez cosultado, o SMS não irá mais ser retornado na chamada deste serviço.
        /// </summary>
        /// <returns>Retorna um objeto do tipo ReceivedResponse com o status e o detalhe das SMS</returns>
        public ReceivedResponse Received()
        {
            ReceivedResponse reply = new ReceivedResponse();
            try
            {
                var response = Deserialize<SmsReceivedResponse>(HttpPost("/received/list"));
                if (response != null)
                {
                    return response.Received;
                }
                else
                {
                    reply.Sms = new Response
                    {
                        StatusCode = "10",
                        StatusDescription = "Error",
                        DetailCode = "999",
                        DetailDescription = "Object ReceivedResponse is null"
                    };
                }
            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);
                reply.Sms = new Response
                {
                    StatusCode = "10",
                    StatusDescription = "Error",
                    DetailCode = e == null ? "999": "901",
                    DetailDescription = e?.Exception?.Message ?? ex.Message
                };
            }
            return reply;
        }

        /// <summary>
        /// Consulta o status de entrega de uma mensagem previamente enviada usando seu identificador id como referência.
        /// <remarks>Importante: a consulta a um SMS fica disponível por até 24 horas após seu envio.</remarks>
        /// </summary>
        /// <param name="id">id da mensagem enviada</param>
        /// <returns>retorna um objeto do tipo SmsStatusReply com o status e o detalhe da operação.</returns>
        public StatusResponse Status(string id)
        {
            StatusResponse reply = new StatusResponse();

            try
            {
                if (string.IsNullOrEmpty(id))
                    throw new ArgumentNullException("id", "Favor informar o Id da mensagem envaida.");

                if (id.Length >= Options.ID_MAX_LENGTH)
                    throw new ArgumentOutOfRangeException("id", "Field ID can not have more than " + Options.ID_MAX_LENGTH + " characters.");

                var response = Deserialize<SmsStatus>(HttpGet(string.Format("/get-sms-status/{0}", id)));
                if (response != null)
                {
                    reply.StatusCode = response.Status.StatusCode;
                    reply.StatusDescription = response.Status.StatusDescription;
                    reply.DetailCode = response.Status.DetailCode;
                    reply.DetailDescription = response.Status.DetailDescription;
                    reply.Id = id;
                    reply.MobileOperatorName = response.Status.MobileOperatorName;
                    reply.Shortcode = response.Status.Shortcode;
                    reply.Received = response.Status.Received;
                }
                else
                {
                    reply.Id = id;
                    reply.StatusCode = "10";
                    reply.StatusDescription = "Error";
                    reply.DetailCode = "999";
                    reply.DetailDescription = "Object getSmsStatusResp is null";
                }
            }
            catch (Exception ex)
            {
                var e = Deserialize<ExceptionResponse>(ex.Message);

                reply.StatusCode = "10";
                reply.StatusDescription = "Error";
                reply.DetailCode = e == null ? "999" : "901";
                reply.DetailDescription = e?.Exception?.Message ?? ex.Message;
                reply.Id = id;
            }
            return reply;
        }



    }
}
