﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Afonsoft.Zenvia.SMS
{
    /// <summary>
    /// Objeto do SMS
    /// </summary>
    [Serializable]
    [XmlRootAttribute(IsNullable = false)]
    public class Request
    {
        /// <summary>
        /// from (opcional) EmpresaABC
        /// </summary>
        [JsonProperty("from")]
        [XmlElement(ElementName = "from", Order = 0, IsNullable = true)]
        public string From { get; set; }

        /// <summary>
        /// to (obrigatório) PAIS+CODE+TEL (5511985368545)
        /// </summary>
        [JsonProperty("to")]
        [XmlElement(ElementName = "to", Order = 1, IsNullable = false)]
        public string To { get; set; }

        /// <summary>
        /// schedule (opcional) - Deve estar no formato definido na ISO 8601(Y-m-dTH:i:s)  (2014-07-18T02:01:23)
        /// </summary>
        [JsonProperty("schedule")]
        [XmlElement(ElementName = "schedule", Order = 2, IsNullable = true)]
        public DateTime? Schedule { get; set; }

        /// <summary>
        /// msg (obrigatório)
        /// </summary>
        [JsonProperty("msg")]
        [XmlElement(ElementName = "msg", Order = 3, IsNullable = false)]
        public string Message { get; set; }

        /// <summary>
        /// id (obrigatório) (SEU_ID_001)
        /// </summary>
        [JsonProperty("id")]
        [XmlElement(ElementName = "id", Order = 4, IsNullable = true)]
        public string Id { get; set; }

        /// <summary>
        /// Poderá ser NONE, FINAL, ou ALL
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public EnumCallback Callback { get; set; } = EnumCallback.NONE;

        /// <summary>
        /// callbackOption
        /// </summary>
        [JsonProperty("callbackOption")]
        [XmlElement(ElementName = "callbackOption", Order = 5, IsNullable = false)]
        public string CallbackOption
        {
            get
            {
                return Enum.GetName(typeof(EnumCallback), Callback);
            }
        }

        /// <summary>
        /// Flash
        /// </summary>
        [JsonProperty("flashSms")]
        [XmlElement(ElementName = "flashSms", Order = 7, IsNullable = false)]
        public bool Flash { get; set; } = false;
       

        /// <summary>
        /// Se sua conta utiliza o recurso de agregador de mensagens (centro de custo, campanha), este parametro deve informar o código do agregador desejado.
        /// </summary>
        [JsonProperty("aggregateId")]
        [XmlElement(ElementName = "aggregateId", Order = 6, IsNullable = true)]
        public string AggregateId { get; set; }
    }

    public enum EnumCallback
    {
        /// <summary>
        /// Não será retornado nenhum callback de status para a mensagem
        /// </summary>
        [Description("NONE")]
        NONE,
        /// <summary>
        /// Serão retornados apenas os callbacks de status final para a mensagem
        /// </summary>
        [Description("FINAL")]
        FINAL,
        /// <summary>
        /// Todos os callbacks de status serão retornados para a mensagem
        /// </summary>
        [Description("ALL")]
        ALL
    }
}
