﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Afonsoft.Zenvia.SMS
{
    [Serializable]
    [XmlRootAttribute(ElementName = "Entity", IsNullable = false)]
    
    public class SmsRequest
    {
        [JsonProperty(PropertyName = "sendSmsRequest")]
        [XmlElement(ElementName = "sendSmsRequest", Order = 0, IsNullable = false)]
        public Request Sms { get; set; }
    }



    [Serializable]
    [XmlRootAttribute(ElementName = "MultiEntity", IsNullable = false)]
    public class SmsMultRequest
    {
        [JsonProperty(PropertyName = "sendSmsMultiRequest")]
        [XmlElement("sendSmsMultiRequest")]
        public MultRequest MultRequest { get; set; }
    }

    public class MultRequest
    {
        /// <summary>
        /// Se sua conta utiliza o recurso de agregador de mensagens (centro de custo, campanha), este parametro deve informar o código do agregador desejado.
        /// </summary>
        [JsonProperty("aggregateId")]
        [XmlElement("aggregateId")]
        public string AggregateId { get; set; }

        [JsonProperty("SmsRequestList")]
        [XmlArray("sendSmsRequestList")]
        public Request[] Sms { get; set; }
    }

}

