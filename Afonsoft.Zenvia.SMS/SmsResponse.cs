﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Afonsoft.Zenvia.SMS
{
    //http://docs.zenviasms.apiary.io/#introduction/
    [Serializable]
    public class SmsResponse
    {
        [JsonProperty(PropertyName = "sendSmsResponse")]
        [XmlElement("sendSmsResponse")]
        public Response Sms { get; set; }
    }

    [Serializable]
    public class SmsCancelResponse
    {
        [XmlElement("cancelSmsResp")]
        [JsonProperty("cancelSmsResp")]
        public Response Sms { get; set; }
    }

    [Serializable]
    public class SmsMultResponse
    {
        [XmlElement("sendSmsMultiResponse")]
        [JsonProperty("sendSmsMultiResponse")]
        public MultResponse MultResponse { get; set; }
    }

    public class MultResponse
    {
        [XmlArray("sendSmsResponseList")]
        [JsonProperty("sendSmsResponseList")]
        public Response[] Sms { get; set; }
    }

    public class SmsReceivedResponse
    {
        [XmlElement("receivedResponse")]
        [JsonProperty("receivedResponse")]
        public ReceivedResponse Received { get; set; }
    }

    public class ReceivedResponse : SmsResponse
    {
        [XmlArray("receivedMessages")]
        [JsonProperty("receivedMessages")]
        public ReceivedMessages[] Messages { get; set; }
    }
}
