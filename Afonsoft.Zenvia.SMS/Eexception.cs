﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace Afonsoft.Zenvia.SMS
{

    public class ExceptionResponse
    {
        [JsonProperty("exception")]
        [XmlElement("exception")]
        public SmsException Exception { get; set; }
    }

    public class SmsException
    {
        [JsonProperty("message")]
        [XmlElement("message")]
        public string Message { get; set; }
    }
}
