﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Afonsoft.Zenvia.SMS
{
    /// <summary>
    /// Em resposta à chamada, a API da Zenvia retornará um status de controle
    /// </summary>
    [Serializable]
    public class Response
    {
        public bool Sucess
        {
            get
            {
                if (string.IsNullOrEmpty(StatusCode))
                    return false;
                return (StatusCode == "00" || StatusCode == "01" || StatusCode == "02" || StatusCode == "03");
            }
        }

        /// <summary>
        /// statusCode
        /// </summary>
        [JsonProperty("statusCode")]
        [XmlElement("statusCode")]
        public string StatusCode { get; set; }

        /// <summary>
        /// statusCode
        /// </summary>
        [JsonProperty("statusDescription")]
        [XmlElement("statusDescription")]
        public string StatusDescription { get; set; }

        /// <summary>
        /// detailCode
        /// </summary>
        [JsonProperty("detailCode")]
        [XmlElement("detailCode")]
        public string DetailCode { get; set; }

        /// <summary>
        /// detailCode
        /// </summary>
        [JsonProperty("detailDescription")]
        [XmlElement("detailDescription")]
        public string DetailDescription { get; set; }

        /// <summary>
        /// parts
        /// </summary>
        [JsonProperty("parts")]
        [XmlArray("parts")]
        public Part[] Parts { get; set; }
    }

    [Serializable]
    public class StatusResponse
    {
        public bool Sucess
        {
            get
            {
                if (string.IsNullOrEmpty(StatusCode))
                    return false;
                return (StatusCode == "00" || StatusCode == "01" || StatusCode == "02" || StatusCode == "03");
            }
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        [XmlElement("id")]
        public string Id { get; set; }

        /// <summary>
        /// Received
        /// </summary>
        [JsonProperty("received")]
        [XmlElement("received")]
        public string Received { get; set; }

        /// <summary>
        /// Shortcode
        /// </summary>
        [JsonProperty("shortcode")]
        [XmlElement("shortcode")]
        public string Shortcode { get; set; }

        /// <summary>
        /// MobileOperatorName
        /// </summary>
        [JsonProperty("mobileOperatorName")]
        [XmlElement("mobileOperatorName")]
        public string MobileOperatorName { get; set; }

        /// <summary>
        /// StatusCode
        /// </summary>
        [JsonProperty("statusCode")]
        [XmlElement("statusCode")]
        public string StatusCode { get; set; }

        /// <summary>
        /// StatusDescription 
        /// </summary>
        [JsonProperty("statusDescription")]
        [XmlElement("statusDescription")]
        public string StatusDescription { get; set; }

        /// <summary>
        /// DetailCode
        /// </summary>
        [JsonProperty("detailCode")]
        [XmlElement("detailCode")]
        public string DetailCode { get; set; }

        /// <summary>
        /// DetailDescription
        /// </summary>
        [JsonProperty("detailDescription")]
        [XmlElement("detailDescription")]
        public string DetailDescription { get; set; }

    }

    public class Part
    {
        /// <summary>
        /// partId
        /// </summary>
        [JsonProperty("partId")]
        [XmlElement("partId")]
        public string Id { get; set; }

        /// <summary>
        /// order
        /// </summary>
        [JsonProperty("order")]
        [XmlElement("order")]
        public int Order { get; set; }
    }

    /// <summary>
    /// Consulta o status de entrega de uma mensagem previamente enviada usando seu identificador id como referência.
    /// </summary>
    [Serializable]
    public class SmsStatus
    {
        [JsonProperty("getSmsStatusResp")]
        [XmlElement("getSmsStatusResp")]
        public StatusResponse Status { get; set; }
    }

}
