﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Afonsoft.Zenvia.SMS
{
    [Serializable]
    public class ReceivedMessages
    {
        [JsonProperty("id")]
        [XmlElement("id")]
        public string Id { get; set; }

        [JsonProperty("dateReceived")]
        [XmlElement("dateReceived")]
        public string DateReceived { get; set; }

        [JsonProperty("mobile")]
        [XmlElement("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("body")]
        [XmlElement("body")]
        public string Body { get; set; }

        [JsonProperty("shortcode")]
        [XmlElement("shortcode")]
        public string Shortcode { get; set; }

        [JsonProperty("mobileOperatorName")]
        [XmlElement("mobileOperatorName")]
        public string MobileOperatorName { get; set; }

        [JsonProperty("mtId")]
        [XmlElement("mtId")]
        public string MtId { get; set; }
    }
}
